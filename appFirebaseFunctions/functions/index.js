const functions = require('firebase-functions');
const fs = require('fs')
const XLSX = require('xlsx')

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });

const admin = require('firebase-admin');
admin.initializeApp();

exports.getExcel = functions.https.onRequest(async (req, res) => {
    // Grab the text parameter.
    const queryStartDate = req.query.start;
    const queryEndDate = req.query.end;

    console.log(queryStartDate)
    console.log(queryEndDate)

    admin
    .database()
    .ref('/responses')
    .once('value')
    .then(snapshot => {
      console.log('get first')
      let reses = snapshot.val()
      console.log(reses)
      for(let res in reses)
        if(!(reses[res].time >= queryStartDate)
          || !(reses[res].time <= queryEndDate))
          delete reses[res]
      console.log(reses)
      let activities = {}
      for(let res in reses){
        let user = reses[res].key
        let type = reses[res].type
        if(activities[user]){
          if(type)
            activities[user].breakdown++
          else
            activities[user].start++
        }
        else{
          activities[user] = {
            breakdown: type?1:0,
            start: !type?1:0,
          }
        }
      }
      let activitiesArray = []
      for(let activity in activities)
        activitiesArray.push({id: activity, ...activities[activity]})
      console.log(activitiesArray)
      admin
      .database()
      .ref('Users/')
      .once('value')
      .then(snapshot2 => {
        let users = snapshot2.val()
        let activitiesArrayNamed = activitiesArray.map(activity => {
          return {name: users[activity.id].name, ...activity}
        });
        console.log("Creating Excel File")
        console.log(activitiesArrayNamed);
        let ws = XLSX.utils.json_to_sheet(activitiesArrayNamed);
        let wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb,ws,"Agents Activity");

        console.log("Writing in Excel")
        const wbout = XLSX.write(wb, {type:'binary', bookType:"xlsx"});
        let file = '/report.xlsx';
        console.log("Writing in File")
        fs.writeFile(file, wbout, err => {
          if(err){
            res.json("Internal Server Error code: 1")
            console.error(err)
            return
          }
          console.log('done saving file')
          res.sendFile(file, err => {
            if(err){
              res.json("Internal Server Error code: 2")
              console.error(err)
              return
            }
          })
        })
        console.log("Writing Done")
      })
      .catch(err => {
        res.json(err);
      });
    })
    .catch(err => {
      res.json(err);
    });
  });
