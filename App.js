/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState, useEffect } from 'react';
import {
  PermissionsAndroid,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Modal,
  Switch,
  TextInput,
  Button,
  TouchableOpacity,
  ToastAndroid,
  ImageBackground,
  Linking,
  Platform,
  Alert,
} from 'react-native';

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';

let states = {
  start: 0,
  login: 1,
  signupBas: 2,
  signupDet: 3,
  loading: 6,
  userPage: 4,
  requesting: 7,
  fixerPage: 5,
  adminPage: 8,
  gatheringData: 9,
}
let responseRadious = 10
let centerPhone = '+12345678901'

import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import Clipboard from '@react-native-community/clipboard';
import Geolocation from 'react-native-geolocation-service';
import 'react-native-console-time-polyfill';
import DatePicker from 'react-native-datepicker'
import Geocoder from 'react-native-geocoding'
import { createOpenLink } from 'react-native-open-maps';
import LinearGradient from 'react-native-linear-gradient';
import Spinner from 'react-native-loading-spinner-overlay';
// import { writeFile, readFile, DocumentDirectoryPath } from 'react-native-fs'
// import XLSX from 'xlsx'

const App: () => React$Node = () => {
  const [locationPrem, setLocationPerm] = useState(false)
  const [initializing, setInitializing] = useState(true)
  const [showPass, setShowPass] = useState(false)
  const [user, setUser] = useState("")
  const [utype, setUType] = useState(0)
  const [email, setEmail] = useState("")
  const [pass, setPass] = useState("")
  const [context, setContext] = useState("")
  const [informText, setInformText] = useState("")
  const [lastReqs, setLastReqs] = useState([])
  const [phone, setPhone] = useState("")
  const [step, setStep] = useState(states.start)
  const [inform, setInform] = useState(false)
  const [userReqHistory, setUserReqHistory] = useState([])
  const [userReqHistoryVisible, setUserReqHistoryVisible] = useState(false);
  const [userReqHistoryReady, setUserReqHistoryReady] = useState(false);
  const [isAvailable, setIsAvailable] = useState(true)
  /////////////// Admin ///////////////////////////
  const [queryStartDate, setQueryStartDate] = useState(new Date(Date.now()))
  const [queryEndDate, setQueryEndDate] = useState(new Date(Date.now()))
  const [adminUsers, setAdminUsers] = useState([])
  const [adminQueryModal, setAdminQueryModal] = useState(false)
  const [adminQueryReady, setAdminQueryReady] = useState(false)
  const [adminQueryResults, setAdminQueryResults] = useState([])

  Geocoder.init("AIzaSyB1bDS9cRTO-N7byi16bBNyF_qwFnQFUzs");

  onAuthStateChanged = (user) => {
    setUser(user);
    if (initializing) setInitializing(false);
  }
  
  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount
  }, [])

  requestLocationPermission = () => {
    console.log('Requesting Permission...')
    return new Promise((resolve, reject) => {
      PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
      .then((finePerm) => {
        switch (finePerm) {
          case /*PermissionsAndroid.RESULTS.GRANTED*/ true:
            setLocationPerm(true);
            resolve()
            break;
          case /*PermissionsAndroid.RESULTS.DENIED*/ false:
            console.warn('Fine Permissions Denied');
            PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
            ).then(perm => {
              if(perm === PermissionsAndroid.RESULTS.GRANTED){
                console.log('Permission Granted!');
                requestLocationPermission()
                .then(() => resolve())
                .catch(err => reject(err));
              }
              else{
                console.warn('Fine Permissions Denied');
                reject({code: 1, msg: 'Fine Permissions Denied'});
              }
            }).catch(err => {
              console.warn(err)
              reject({code: 2, msg: err});
            });
            break;
          default:
            setLocationPerm(true);
            reject({
              code: 2, 
              msg: "The fine permission is either blocked {"
              + (finePerm === PermissionsAndroid.RESULTS.BLOCKED)
              + "} or unavailable {"
              + (finePerm === PermissionsAndroid.RESULTS.UNAVAILABLE)
              + "}"
              + ` the real value is {${finePerm}}`
            });
            break;
        }
      })
      .catch(err => reject({code: 3, msg: err}));
    });
  }

  useEffect(() => {
    if(!locationPrem)
      requestLocationPermission()
      .then(() => console.log("done"))
      .catch((err) => console.warn(err))
  })

  createUser = () => {
    setStep(states.loading);
    auth()
    .createUserWithEmailAndPassword(email, pass)
    .then(() => {
      console.log('User account created & signed in!');
      setStep(states.signupDet);
    })
    .catch(error => {
      if (error.code === 'auth/email-already-in-use') {
        console.log('That email address is already in use!');
        console.log("Try logingg in...")
        auth()
        .signInWithEmailAndPassword(email, pass)
        .then(() => {
          console.log('User account signed in!');
          setStep(states.signupDet);
        })
        .catch(error => {
          let message = '';
          if (error.code === 'auth/wrong-password')
            message = 'Email existed.\nAnd passwrod which you provided is incorrect.'
          else 
            message = 'Error! Contact Admin'
          ToastAndroid.showWithGravityAndOffset(
            message,
            ToastAndroid.LONG,
            ToastAndroid.BOTTOM,
            25,
            50
          );
          console.error(message);
          setStep(states.signupBas);
        });
      }
      if (error.code === 'auth/invalid-email') {
        console.log('That email address is invalid!');
        ToastAndroid.showWithGravityAndOffset(
          "The email address is invalid!",
          ToastAndroid.LONG,
          ToastAndroid.BOTTOM,
          25,
          50
        );
        setStep(states.signupBas);
      }
      if (error.code === 'auth/weak-password') {
        console.log(error.message);
        ToastAndroid.showWithGravityAndOffset(
          "Your Password is weak.\nIt should be at least 6 character",
          ToastAndroid.LONG,
          ToastAndroid.BOTTOM,
          25,
          50
        );
        setStep(states.signupBas);
      }
      else{
        console.log(error);
        setStep(states.signupBas);
      }
    });
  }

  loginUser = () => {
    setStep(states.loading);
    console.log("Logingg in...")
    auth()
    .signInWithEmailAndPassword(email, pass)
    .then(() => {
      console.log('User account signed in!');
      setStep(states.loading);
      setTimeout(fetchInformation,1000);
    })
    .catch(error => {
      let message = '';
      if(error.code === 'auth/invalid-email')
        message = 'Invalid E-Mail Address!'
      else if (error.code === 'auth/user-not-found')
        message = 'User not Found!'
      else if (error.code === 'auth/wrong-password')
        message = 'Wrong Password!'
      else 
        message = 'Error! Contact Admin'
      ToastAndroid.showWithGravityAndOffset(
        message,
        ToastAndroid.LONG,
        ToastAndroid.BOTTOM,
        25,
        50
      );
      console.error(message);
      setStep(states.login);
    });
  }

  fetchInformation = () => {
    console.log('checking for user type...');
    database()
    .ref('Users/' + user.uid)
    .once('value')
    .then(snapshot => {
      try{
        setPhone(snapshot.val().phone);
        setContext(snapshot.val().name);
        setUType(snapshot.val().type);
        console.log('User inofrmation fetched!');
        if(snapshot.val().type === 1){
          readRequestDatabaseWhole(true);
        }
        else if(snapshot.val().type === 0){
          setStep(states.userPage);
        }
        else if(snapshot.val().type === 3){
          fetchAdminData();
        } 
      } catch(e) {
        if (e instanceof TypeError)
          logoutUser();
        else
          console.error(e)
      }
    }).catch(err => {
      console.warn(err);
    });
  }

  fetchAdminData = () => {
    database()
    .ref('/Users')
    .once('value')
    .then(snapshot => {
      let UsersT = snapshot.val()
      let Users = []
      for(UserIDx in UsersT){
        Users.push({
          id: UserIDx,
          ...UsersT[UserIDx]
        }); 
      }
      setAdminUsers(Users);
      setStep(states.adminPage);
    })
    .catch(err => console.warn(err));
  }

  logoutUser = () => {
    auth()
    .signOut()
    .then(() => {
      console.log('User signed out!');
      setUser(undefined);
      setStep(states.start);
    });
  }

  addName = () =>{
    setStep(states.loading);
    console.log('Creating user profile in real-time database...');
    database()
    .ref('Users/' + user.uid)
    .set({
      name: context,
      email: email,
      phone: phone,
      type: utype?1:0,
    })
    .then(() => {
      console.log('User profile set.');
      fetchInformation();
    })
    .catch(err => {
      console.warn(err);
      ToastAndroid.showWithGravityAndOffset(
        "There is a problem. Please try again later.",
        ToastAndroid.LONG,
        ToastAndroid.BOTTOM,
        25,
        50
      );
      setStep(states.signupDet);
    });
  }

  requestRecovery = (fixType) => {
    setStep(states.requesting);
    getLocation()
    .then(pos => {
      console.log('Requesting A Recovery');
      database()
      .ref('/Users/' + user.uid)
      .once('value')
      .then(snapshot => {
        console.log('User Data Fetched...');
        database()
        .ref('/requests/')
        .push()
        .set({
          requester: user.uid,
          requesterName: snapshot.val().name,
          phoneNumber: snapshot.val().phone,
          type: fixType,
          time: Date.now(),
          latitude: pos.coords.latitude,
          longitude: pos.coords.longitude
        })
        .then(() => {
          setStep(states.userPage);
          ToastAndroid.showWithGravityAndOffset(
            "Recovery Requested...\nWait for the call!",
            ToastAndroid.LONG,
            ToastAndroid.BOTTOM,
            25,
            50
          );
          console.log('Requested');
        })
        .catch(err => {
          setStep(states.userPage);
          ToastAndroid.showWithGravityAndOffset(
            "Error!",
            ToastAndroid.LONG,
            ToastAndroid.BOTTOM,
            25,
            50
          );
          console.log(err);
        });
      })
      .catch(err => {
        setStep(states.userPage);
        ToastAndroid.showWithGravityAndOffset(
          "An Error Accured!",
          ToastAndroid.LONG,
          ToastAndroid.BOTTOM,
          25,
          50
        );
        console.log(err);
      });
    })
    .catch(err => {
      if(err === 0){
        console.warn("Try Again");
        ToastAndroid.showWithGravityAndOffset(
          "Please Try Again",
          ToastAndroid.LONG,
          ToastAndroid.BOTTOM,
          25,
          50
        );
        setStep(states.userPage);
      }
      else{
        ToastAndroid.showWithGravityAndOffset(
          "There is a error.\nTry again Later.",
          ToastAndroid.LONG,
          ToastAndroid.BOTTOM,
          25,
          50
        );
        console.warn(err);
        setStep(states.userPage);
      }
    })
  }

  continueReadingReq = (snapshot, pos) => {
    console.time("Loading Agent Values")
    database().ref('responses/').limitToLast(50).once('value', snapshot2 => {
      database()
      .ref('responses/')
      .orderByChild('key')
      .equalTo(user.uid)
      .once('value', snapshot3 => {
        console.timeEnd("Loading Agent Values")
        console.log(snapshot3.val())
        console.log('Requests data');
        let idx = null;
        let vals = snapshot3.val()
        for(let i in vals) if(!vals[i].done) idx = i
        if(!idx){
          vals = snapshot.val()
          let newReqs = []
          let resKeys = (!snapshot2 || !snapshot2.val())?([]):(Object.keys(snapshot2.val()));
          console.time("Processing Agent Requests")
          for (let element in vals){
            let responded = resKeys.includes(element);
            let responder = responded ? (snapshot2.val()[element].key === user.uid) : (false);
            if (!responded)
              newReqs.push({
                ...vals[element], 
                time: new Date(vals[element].time), 
                key: vals[element].time + element, 
                id: element,
                responded: responded,
                responder: responder,
                latitude: pos.coords.latitude,
                longitude: pos.coords.longitude,
                distance: calcDistance(
                  pos.coords.latitude,
                  pos.coords.longitude,
                  vals[element].latitude,
                  vals[element].longitude
                )
              });
          }
          newReqs = newReqs.sort((a, b) => {
            if(a.responded && !b.responded)
              return 1
            else if (!a.responded && b.responded)
              return -1
            else
              return (parseInt(a.time.getTime()) > parseInt(b.time.getTime())) ? -1 : (parseInt(a.time.getTime()) < parseInt(b.time.getTime()) ? 1 : 0)
          });
          newReqs = newReqs.filter((req) => req.distance <= responseRadious);
          console.timeEnd("Processing Agent Requests")
          setIsAvailable(true);
          setLastReqs(newReqs);
          setStep(states.fixerPage);
        }
        else{
          vals = snapshot.val()
          let newReqs = [{
            ...vals[idx], 
            time: new Date(vals[idx].time), 
            key: vals[idx].time + idx, 
            id: idx,
            latitude: pos.coords.latitude,
            longitude: pos.coords.longitude,
            distance: calcDistance(
              pos.coords.latitude,
              pos.coords.longitude,
              vals[idx].latitude,
              vals[idx].longitude
            )
          }];
          setIsAvailable(false);
          setLastReqs(newReqs);
          setStep(states.fixerPage);
        }
      }).catch(err => {
        console.warn('in reading /responses');
        console.error(err)
      })
    }).catch(err => {
      console.warn('in reading /responses');
      console.error(err)
    })
  }

  readRequestDatabaseWhole = (realtime = true) => {
    console.log('start setting database. realtime: ' + realtime)
    getLocation()
    .then(pos => {
      let reqRefer = database()
                      .ref('requests/')
                      .orderByChild('type')
                      .equalTo(true)
                      .limitToLast(10);
      console.log('setting database')
      if(realtime){
        reqRefer 
        .on('value', snapshot => {
          continueReadingReq(snapshot, pos);
        })
      }
      else{
        reqRefer
        .once('value')
        .then(snapshot => {
          continueReadingReq(snapshot, pos);
        }).catch(err =>{
          console.warn('in reading /requests');
          console.warn(err)
        });
      }
      console.log('Request Database read Handler set');
    })
    .catch(err => {
      if(err === 0){
        console.warn("Try Again");
        ToastAndroid.showWithGravityAndOffset(
          "Please Try Again.\nClose the app and reopen it.",
          ToastAndroid.LONG,
          ToastAndroid.BOTTOM,
          25,
          50
        );
      }
      else{
        ToastAndroid.showWithGravityAndOffset(
          "Can not fetch your location.\nTry again Later.",
          ToastAndroid.LONG,
          ToastAndroid.BOTTOM,
          25,
          50
        );
        console.warn(err)
      }
    });
  }

  openMap = (lat, lng) => {
    const scheme = Platform.select({ ios: 'maps:0,0?q=', android: 'geo:0,0?q=' });
    const latLng = `${lat},${lng}`;
    const label = 'Customer';
    const url = Platform.select({
      ios: `${scheme}${label}@${latLng}`,
      android: `${scheme}${latLng}(${label})`
    });
    Linking.canOpenURL(url)
    .then(supported => {
      if(supported){
        Linking.openURL(url);
      }
      else{
        ToastAndroid.showWithGravityAndOffset(
          "Your phone doesn't support map location.",
          ToastAndroid.LONG,
          ToastAndroid.BOTTOM,
          25,
          50
        );
      }
    })
    .catch(err => {
      console.error(err)
    });
  }

  acceptRecovery = (id, type, lat, long) => {
    console.log('Accepting recovery ' + id)
    let json = {}
    json[id] = user.uid
    database()
    .ref('/responses/' + id)
    .set({ 
      key: user.uid, 
      type: type,
      done: false,
      time: Date.now(),
    })
    .then(() => {
      console.log('Accepted')
      database()
      .ref('/requests/' + id)
      .once('value')
      .then( snapshot => {
        console.time("Fetching Address Takes")
        setInformText({
          context: "Congratulations!\nRecovery Accepted\n\nClient Name:\n"
            + snapshot.val().requesterName
            + "\n\nClient's Phone number is:\n"
            + snapshot.val().phoneNumber,
          address: "",
          location: {
            latitude: lat, 
            longitude: long,
          },
          mode: 2,
          phone: snapshot.val().phoneNumber,
          status: true
        });
        setInform(true);
        readRequestDatabaseWhole(false);
        // Geocoder.from(lat, long)
        // .then(json => {
        //   console.timeEnd("Fetching Address Takes")
        //   setInformText({
        //     context: "Congratulations!\nRecovery Accepted\n\nClient Name:\n"
        //       + snapshot.val().requesterName
        //       + "\nClient's Phone number is:\n"
        //       + snapshot.val().phoneNumber,
        //     address: json.results[0].address_components[0],
        //     phone: snapshot.val().phoneNumber,
        //     status: true
        //   });
        //   setInform(true);
        //   readRequestDatabaseWhole(false);
        // })
        // .catch(err => {
        //   console.warn(err)
        //   setInformText({
        //     context: "Congratulations!\nRecovery Accepted\n\nClient Name:\n"
        //       + snapshot.val().requesterName
        //       + "\nClient's Phone number is:\n"
        //       + snapshot.val().phoneNumber,
        //     address: "",
        //     phone: snapshot.val().phoneNumber,
        //     status: true
        //   });
        //   setInform(true);
        //   readRequestDatabaseWhole(false);
        // })
      })
    })
    .catch(err => {
      if(err.code === 'database/permission-denied'){
        console.warn(err);
        setInformText({
          context: "Already Responded by someone else!",
          status: false
        });
        setInform(true);
      }
      else{
        console.warn(err.code)
      }
    });
  }

  askToAcceptRecovery = (name, id, type, latitude, longitude) => {
    setInformText({
      context: "Client Name:\n"
        + name
        + "\n\nFor client location press the 'show in Map' button",
      address: "",
      id: id,
      type: type,
      location: {
        latitude: latitude, 
        longitude: longitude,
      },
      mode: 1,
      status: true
    });
    setInform(true);
  }

  getLocation = () => {
    return new Promise((resolve, reject) => {
      console.log('getting location...');
      Geolocation.getCurrentPosition(pos => {
        resolve(pos);
        console.log(pos);
      }, err => {
        if(err.code === 1 
          && err.message === 'Location permission not granted.'){
          console.warn('Location permission not granted.')
          requestLocationPermission()
          .then(() => {
            Geolocation.getCurrentPosition(pos => {
              resolve(pos);
              console.log(pos);
            }, err2 => reject(err2));
          })
          .catch(err2 => reject(err2))  
        }
        reject(err);
      },{
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 10000,
      });
    })
  }

  calcDistance = (lat1, lon1, lat2, lon2) => {
    let p = 0.017453292519943295;
    let c = Math.cos;
    let a = 0.5 - c((lat2 - lat1) * p)/2 + 
            c(lat1 * p) * c(lat2 * p) * 
            (1 - c((lon2 - lon1) * p))/2;
  
    return 12742 * Math.asin(Math.sqrt(a));
  }

  changeUserType = (id, type) => {
    database()
    .ref('/Users/' + id)
    .update({ type: type?1:0 })
    .then(() => {
      fetchAdminData();
    })
    .catch(err => {
      console.warn(err);
    })
  }

  deleteUser = (id) => {
    auth
    let load = {};
    load[id] = null;
    database()
    .ref('/Users')
    .update(load)
    .then(() => {
      fetchAdminData();
    })
    .catch(err => {
      console.warn(err);
    })
  }

  getUserReqHistory = () => {
    setUserReqHistoryVisible(true)
    setUserReqHistoryReady(false)
    console.time("GetUserReqHistory")
    database()
    .ref('/requests')
    .orderByChild('requester')
    .equalTo(user.uid)
    .on('value', snapshot => {
      getLocation()
      .then(pos => {
        console.timeEnd("GetUserReqHistory")
        let vals = snapshot.val();
        let reqs = [];
        for(req in vals){
          reqs.push({
            distance: calcDistance(
              pos.coords.latitude,
              pos.coords.longitude,
              vals[req].latitude,
              vals[req].longitude
            ),
            time: new Date(vals[req].time),
            type: vals[req].type,
          });
        }
        setUserReqHistory(reqs);
        setUserReqHistoryReady(true);
      })
      .catch(err => {
        if(err === 0){
          console.warn("Try Again");
          ToastAndroid.showWithGravityAndOffset(
            "Please Try Again.\nClose the app and reopen it.",
            ToastAndroid.LONG,
            ToastAndroid.BOTTOM,
            25,
            50
          );
        }
        else{
          ToastAndroid.showWithGravityAndOffset(
            "Can not fetch your location.\nTry again Later.",
            ToastAndroid.LONG,
            ToastAndroid.BOTTOM,
            25,
            50
          );
          console.warn(err)
        }
      });
    });
  }

  getAgentsActivity = () => {
    setAdminQueryModal(true)
    setAdminQueryReady(false)
    console.time("GetAgentsActivity")
    database()
    .ref('/responses')
    .orderByChild('time')
    .startAt((new Date(queryStartDate)).getTime())
    .endAt((new Date(queryEndDate)).getTime())
    .once('value')
    .then(snapshot => {
      console.log("Processing Data..")
      let reses = snapshot.val()
      let activities = {}
      for(let res in reses){
        let user = reses[res].key
        let type = reses[res].type
        if(activities[user]){
          if(type)
            activities[user].breakdown++
          else
            activities[user].start++
        }
        else{
          activities[user] = {
            breakdown: type?1:0,
            start: !type?1:0,
          }
        }
      }
      let activitiesArray = []
      for(let activity in activities)
        activitiesArray.push({id: activity, ...activities[activity]})
      console.log("Processing Data [COMPELETED]")
      database()
      .ref('Users/')
      .once('value')
      .then(snapshot2 => {
        let users = snapshot2.val()
        let activitiesArrayNamed = activitiesArray.map(activity => {
          return {name: users[activity.id].name, ...activity}
        });
        console.log(activitiesArrayNamed)
        setAdminQueryResults(activitiesArrayNamed)
        setAdminQueryReady(true)
      })
      .catch(err => {
        console.error(err)
        setAdminQueryReady(false)
        setAdminQueryModal(false);
      });
    })
    .catch(err => {
      console.error(err)
    })
  }

  adminCopyAgentsData = () => {
    if(adminQueryResults.length > 0){
      let data = [{name:"name",id:"id",breakdown:"Breakdown",start:"Doesn't Start"},...adminQueryResults]
      data = data.map( elem => ""+elem.name+","+elem.id+","+elem.breakdown+","+elem.start)
      data = data.join('\n')
      Clipboard.setString(data)
    }
    else{
      console.log("Modal has been closed.");
      setAdminQueryModal(false);
      setAdminQueryReady(false);
    }
  }

  callCenter = (phone = centerPhone) => {
    Linking.canOpenURL(`tel:${phone}`)
    .then(supported => {
      if(supported){
        return Linking.openURL(`tel:${phone}`)
      }
      else{
        ToastAndroid.showWithGravityAndOffset(
          "Your phone doesn't support phone call.",
          ToastAndroid.LONG,
          ToastAndroid.BOTTOM,
          25,
          50
        );
      }
    })
    .catch(err => {
      console.error(err)
    })
  }

  doneRequest = () => {
    Alert.alert(
      "Job is Done",
      "Are you sure you want to mark current job as done?",
      [
        {
          text: "No",
          onPress: () => console.log("canceled"),
          style: 'cancel',
        },
        {
          text: "Yes",
          onPress: () => {
            setStep(states.loading)
            database()
            .ref('/responses/' + lastReqs[0].id + '/done')
            .set(true)
            .then(() => {
              ToastAndroid.showWithGravity(
                "Operation was successfull",
                ToastAndroid.LONG,
                ToastAndroid.BOTTOM
              )
              readRequestDatabaseWhole(false)
            })
            .catch(err => {
              console.error(err)
              ToastAndroid.showWithGravity(
                "Operation Failed",
                ToastAndroid.LONG,
                ToastAndroid.BOTTOM
              )
            })
          },
          style: 'default',
        }
      ],
      {cancelable: true}
    )
  }

  cancelRequest = () => {
    Alert.alert(
      "Cancel the Job",
      "Are you sure you want to cancel the current in-progress job?",
      [
        {
          text: "No",
          onPress: () => console.log("canceled"),
          style: 'cancel',
        },
        {
          text: "Yes",
          onPress: () => {
            setStep(states.loading)
            let json = {}
            json[lastReqs[0].id] = null
            database()
            .ref('/responses/')
            .set(json)
            .then(() => {
              ToastAndroid.showWithGravity(
                "Operation was successfull",
                ToastAndroid.LONG,
                ToastAndroid.BOTTOM
              )
              readRequestDatabaseWhole(false)
            })
            .catch(err => {
              console.error(err)
              ToastAndroid.showWithGravity(
                "Operation Failed",
                ToastAndroid.LONG,
                ToastAndroid.BOTTOM
              )
            })
          },
          style: 'default',
        }
      ],
      {cancelable: true}
    )
  }

  /* ****************** Views ****************** */

  if(step === states.loading){
    return <>
      <LinearGradient
        style={{height: "101%",alignItems: 'center',justifyContent: 'center',}}
        colors={['#001642', '#002570']}
      >
        <Spinner
          visible={step === states.loading}
          textContent={'Loading...'}
          textStyle={{color:'white'}}
        />
      </LinearGradient>
    </>
  }

  if(step === states.requesting){
    return <>
      <LinearGradient
        style={{height: "101%",alignItems: 'center',justifyContent: 'center',}}
        colors={['#001642', '#002570']}
      >
        <Spinner
          visible={step === states.loading}
          textContent={'Requesting'}
          textStyle={{color:'white'}}
        />
      </LinearGradient>
    </>
  }

  if(initializing){
    return <>
      <View style={{height: 350}}>
        <Text style={{marginTop: 320, textAlign: 'center'}}>Initializing</Text>
      </View>
    </>
  }

  if((!user && [states.start, states.login, states.signupBas].includes(step)) || (step === states.signupDet)){
    return (<>
      <Modal
        animationType={'slide'}
        visible={step === states.login}
        onRequestClose={() => {
          setStep(states.start)
        }}
        transparent={false}
      >
        <ImageBackground
          source={require('/home/user9886/Project/React-Native/recover-me/Pictures/mechanic_sign_in.jpg')}
          style={loginPage.background.style.image}
        >
          <Text style={loginPage.main.text.style.context}>LOGIN</Text>
          <View style={loginPage.main.inputs.style.container}>
            <TextInput style={loginPage.main.inputs.style.input} placeholder={"E-Mail"} value={email} onChangeText={txt => setEmail(txt)}/>
            <TextInput style={loginPage.main.inputs.style.input} placeholder={"Password"} value={pass} onChangeText={txt => setPass(txt)}/>
            <View style={loginPage.main.inputs.buttons.style.container}>
              <Text style={loginPage.main.inputs.buttons.style.text}>Password Visibility</Text>
              <Switch 
                trackColor={loginPage.main.inputs.buttons.switch.track}
                style={loginPage.main.inputs.buttons.style.switch}
                value={showPass} 
                onValueChange={() => setShowPass(!showPass)}
              />
              <Text style={loginPage.main.inputs.buttons.style.button} onPress={loginUser}>LOGIN</Text>
            </View>
          </View>
          {/* <Text style={{padding: 10}}>Click here if you want to sign-up.</Text> */}
        </ImageBackground>
      </Modal>
      <Modal
        animationType={'slide'}
        visible={step === states.signupBas}
        onRequestClose={() => {
          setStep(states.start)
        }}
        transparent={false}
      >
        <ImageBackground
          source={require('/home/user9886/Project/React-Native/recover-me/Pictures/mechanic_sign_in.jpg')}
          style={signupPage.background.style.image}
        >
          <Text style={signupPage.main.text.style.context}>Create a new account</Text>
          <View style={signupPage.main.inputs.style.container}>
            <TextInput style={signupPage.main.inputs.style.input} placeholder={"E-Mail"} value={email} onChangeText={txt => setEmail(txt)}/>
            <TextInput style={signupPage.main.inputs.style.input} placeholder={"Password"} value={pass} onChangeText={txt => setPass(txt)}/>
            <View style={signupPage.main.inputs.buttons.style.container}>
              <Text style={signupPage.main.inputs.buttons.style.button} onPress={createUser}>Continue</Text>
            </View>
          </View>
        </ImageBackground>
      </Modal>
      <Modal
        animationType={'slide'}
        visible={step === states.signupDet}
        onRequestClose={() => {
          setStep(states.signupBas)
        }}
        transparent={false}
      >
        <ImageBackground
          source={require('/home/user9886/Project/React-Native/recover-me/Pictures/mechanic_sign_in.jpg')}
          style={signupPage.background.style.image}
        >
          <Text style={signupPage.main.text.style.context}>Create a new account</Text>
          <View style={signupPage.main.inputs.style.container}>
            <TextInput style={signupPage.main.inputs.style.input} placeholder={"Full Name"} value={context} onChangeText={txt => setContext(txt)}/>
            <TextInput style={signupPage.main.inputs.style.input} dataDetectorTypes={'phoneNumber'} keyboardType={"phone-pad"} placeholder={"Phone Number with Country Code"} value={phone} onChangeText={txt => setPhone(txt)}/>
            <View style={signupPage.main.inputs.buttons.style.container}>
              <Text style={signupPage.main.inputs.buttons.style.text}>I am {utype ? "an Agent" : "a Client"}</Text>
              <Switch
                trackColor={signupPage.main.inputs.buttons.switch.track}
                style={signupPage.main.inputs.buttons.style.switch}
                onValueChange={() => setUType(!utype)}
                value={utype}
              />
            </View>
            <Text style={signupPage.main.inputs.buttons.style.button} onPress={addName}>Finish</Text>
          </View>
        </ImageBackground>
      </Modal>
      <ImageBackground
        source={require('/home/user9886/Project/React-Native/recover-me/Pictures/mechanic_first_page.jpg')}
        style={startPage.background.style.image}
      >
        <Text style={startPage.main.text.style.context}>ALPHA!{"\n"}RECOVER ME!</Text>
        <View style={startPage.main.buttons.style.container}>
          <Text onPress={() => {setEmail("");setPass("");setStep(states.login)}} style={startPage.main.buttons.style.buttons}>LOGIN</Text>
          <Text onPress={() => {setEmail("");setPass("");setContext("");setPhone("");setStep(states.signupBas)}} style={startPage.main.buttons.style.buttons}>SIGN UP</Text>
        </View>
      </ImageBackground>
    </>)
  }
  
  if(false){//!user && step === states.login){
    return (
      <>
        <ImageBackground
          source={require('/home/user9886/Project/React-Native/recover-me/Pictures/mechanic_sign_in.jpg')}
          style={loginPage.background.style.image}
        >
          <Text style={loginPage.main.text.style.context}>LOGIN</Text>
          <View style={loginPage.main.inputs.style.container}>
            <TextInput style={loginPage.main.inputs.style.input} placeholder={"E-Mail"} value={email} onChangeText={txt => setEmail(txt)}/>
            <TextInput style={loginPage.main.inputs.style.input} placeholder={"Password"} value={pass} onChangeText={txt => setPass(txt)}/>
            <View style={loginPage.main.inputs.buttons.style.container}>
              <Text style={loginPage.main.inputs.buttons.style.text}>Password Visibility</Text>
              <Switch 
                trackColor={loginPage.main.inputs.buttons.switch.track}
                style={loginPage.main.inputs.buttons.style.switch}
                value={showPass} 
                onValueChange={() => setShowPass(!showPass)}
              />
              <Text style={loginPage.main.inputs.buttons.style.button} onPress={loginUser}>LOGIN</Text>
            </View>
          </View>
          {/* <Text style={{padding: 10}}>Click here if you want to sign-up.</Text> */}
        </ImageBackground>
      </>
    )
  }
  
  if(false){//!user && step === states.signupBas){
    return (
      <>
        <ImageBackground
          source={require('/home/user9886/Project/React-Native/recover-me/Pictures/mechanic_sign_in.jpg')}
          style={signupPage.background.style.image}
        >
          <Text style={signupPage.main.text.style.context}>Create a new account</Text>
          <View style={signupPage.main.inputs.style.container}>
            <TextInput style={signupPage.main.inputs.style.input} placeholder={"E-Mail"} value={email} onChangeText={txt => setEmail(txt)}/>
            <TextInput style={signupPage.main.inputs.style.input} placeholder={"Password"} value={pass} onChangeText={txt => setPass(txt)}/>
            <View style={signupPage.main.inputs.buttons.style.container}>
              <Text style={signupPage.main.inputs.buttons.style.button} onPress={createUser}>Continue</Text>
            </View>
          </View>
          {/* <TextInput textContentType={"none"} placeholder={"Your EMail"} value={email} onChangeText={txt => setEmail(txt)}/>
          <TextInput textContentType={"none"} placeholder={"Your Password"} value={pass} onChangeText={txt => setPass(txt)}/>
          <Button title={"Continue"} onPress={createUser}/>
          <Text style={{padding: 10}} onPress={() => setStep(states.login)}>Click here if you want to login.</Text> */}
        </ImageBackground>
      </>
    )
  }

  if(false){//step === states.signupDet){
    return (
      <>
        <ImageBackground
          source={require('/home/user9886/Project/React-Native/recover-me/Pictures/mechanic_sign_in.jpg')}
          style={signupPage.background.style.image}
        >
          <Text style={signupPage.main.text.style.context}>Create a new account</Text>
          <View style={signupPage.main.inputs.style.container}>
            <TextInput style={signupPage.main.inputs.style.input} placeholder={"Full Name"} value={context} onChangeText={txt => setContext(txt)}/>
            <TextInput style={signupPage.main.inputs.style.input} dataDetectorTypes={'phoneNumber'} keyboardType={"phone-pad"} placeholder={"Phone Number with Country Code"} value={phone} onChangeText={txt => setPhone(txt)}/>
            <View style={signupPage.main.inputs.buttons.style.container}>
              <Text style={signupPage.main.inputs.buttons.style.text}>I am {utype ? "an Agent" : "a Client"}</Text>
              <Switch
                trackColor={signupPage.main.inputs.buttons.switch.track}
                style={signupPage.main.inputs.buttons.style.switch}
                onValueChange={() => setUType(!utype)}
                value={utype}
              />
            </View>
            <Text style={signupPage.main.inputs.buttons.style.button} onPress={addName}>Finish</Text>
          </View>
        </ImageBackground>
      </>
    )
  }

  if(user && step === states.start){
    setStep(states.loading)
    fetchInformation();
  }

  if(user && step === states.userPage){
    return (
      <>
        <View>
          <View style={userPage.header.style.container}>
            <View style={userPage.header.logoutButton.style.container}>
              <TouchableOpacity 
                style={userPage.header.logoutButton.style.area}
                onPress={logoutUser}>
                <Text style={userPage.header.logoutButton.style.text}>
                  {'LOG OUT'}
                </Text>
              </TouchableOpacity>
            </View>
            <View style={userPage.header.userHistoryButton.style.container}>
              <TouchableOpacity 
                style={userPage.header.userHistoryButton.style.area}
                onPress={getUserReqHistory}>
                <Text style={userPage.header.userHistoryButton.style.text}>
                  {'History'}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <ImageBackground 
            source={require('/home/user9886/Project/React-Native/recover-me/Pictures/main_background.jpg')}
            style={userPage.main.background.style.image}
          >
            <View style={userPage.main.background.style.container}>
              <View style={userPage.main.buttons.right.style.container}>
                <TouchableOpacity 
                  style={userPage.main.buttons.style.area}
                  onPress={callCenter}>
                  <Text style={userPage.main.buttons.style.context}>
                    {'Accident'}
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={userPage.main.buttons.left.style.container}>
                <TouchableOpacity 
                  style={userPage.main.buttons.style.area}
                  onPress={() => requestRecovery(true)}>
                  <Text style={userPage.main.buttons.style.context}>
                    {'Car\nBreakdown'}
                  </Text>
                </TouchableOpacity>
              </View>
              {/* <View style={styles.revoceryTypeContainer}>
                <Text style={styles.revoceryTypeText}>{fixType ? 'Breakdown' : 'Doesn\'t Start'}</Text>
                <Switch
                  style={styles.revoceryTypeSwith}
                  onValueChange={() => setFixType(!fixType)}
                  value={fixType}
                />
              </View> */}
            </View>
          </ImageBackground>
          <Modal
            animationType="fade"
            transparent={true}
            visible={userReqHistoryVisible}
            onRequestClose={() => {
              console.log("Modal has been closed.");
              setUserReqHistoryReady(false);
              setUserReqHistoryVisible(false);
            }}
          >
            <View style={userPage.userHistory.style.modal}>
              <View style={userPage.userHistory.style.scroll}>
              {userReqHistoryVisible ? (<>
                <ScrollView style={userPage.userHistory.style.scrollContainer}>
                  {userReqHistory.map(req => (
                    <View 
                      key={req.time.getTime()} 
                      style={userPage.userHistory.style.container}
                    >
                      <View style={userPage.userHistory.style.area}>
                        <Text style={userPage.userHistory.style.type}>
                          {req.type ? ("Breakdown") : ("Start Problem")}
                        </Text>
                        <Text style={userPage.userHistory.style.distance}>
                          {
                            req.distance >= 1 ?
                            parseInt(req.distance)
                            + 'Km and ' 
                            + (parseInt(req.distance*1000) % 1000) 
                            + 'm'
                            :
                            (parseInt(req.distance*1000) % 1000) 
                            + 'm'
                          }
                        </Text>
                        <Text style={userPage.userHistory.style.date}>
                          {req.time.toLocaleDateString('en-GB') + ' - ' + req.time.toLocaleTimeString('en-GB')}
                        </Text>
                      </View>
                    </View>
                  ))}
                </ScrollView>
              </>) : (<>
                <Text>Loading</Text>
              </>)}
              </View>
            </View>
          </Modal>
        </View>
      </>
    )
  }

  if(user && step === states.fixerPage){
    if(lastReqs.length){
      return (
        <>
          <Modal
            animationType="slide"
            transparent={true}
            visible={inform}
            onRequestClose={() => {
              console.log("Modal has been closed.");
              setInform(false);
            }}
          >
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
                <Text style={{textAlign:"center", marginBottom: 10}}>{informText.context}</Text>
                <Text style={{textAlign:"center", marginBottom: 10}}>{informText.address}</Text>
                <View style={{flexDirection: 'row'}}>
                  {informText.mode === 1 ? (
                    <Button 
                      style={{flex: 1,}} 
                      onPress={() => acceptRecovery(
                        informText.id, 
                        informText.type, 
                        informText.location.latitude, 
                        informText.location.longitude
                      )} 
                      title={"Accept Recovery"}
                    />
                  ) : (
                    <Button 
                      style={{flex: 1,}} 
                      onPress={() => informText.status ? (
                          callCenter(informText.phone)
                        ) : (
                          setInform(false)
                        )} 
                      title={informText.status ? 'Copy Phone Number' : 'Close'}
                    />
                  )}
                  {informText.status ? (<Text style={{marginRight: 10, marginLeft: 10}}></Text>) : null}
                  {informText.status ? (<Button disabled={!informText.status} style={{flex: 1}} onPress={() => openMap(informText.location.latitude, informText.location.longitude)} title={"Show in Map"}/>) : null}
                </View>
              </View>
            </View>
          </Modal>
          <View style={fixerPage.header.style.container}>
            <View style={fixerPage.header.logoutButton.style.container}>
              <TouchableOpacity 
                style={fixerPage.header.logoutButton.style.area}
                onPress={logoutUser}>
                <Text style={fixerPage.header.logoutButton.style.text}>
                  {'LOG OUT'}
                </Text>
              </TouchableOpacity>
            </View>
            <View style={fixerPage.header.userHistoryButton.style.container}>
              <TouchableOpacity 
                style={fixerPage.header.userHistoryButton.style.area}
                onPress={() => {
                  setStep(states.loading)
                  readRequestDatabaseWhole(false)
                }}
              >
                <Text style={fixerPage.header.userHistoryButton.style.text}>
                  {'Refresh'}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          {isAvailable ? (
            <ScrollView style={{marginTop: 20}}>
              {lastReqs.map(req => (
                <View key={req.key} style={fixerPage.main.style.container}>
                  <TouchableOpacity
                    style={fixerPage.main.style.area}
                    onPress={() => askToAcceptRecovery(req.requesterName ,req.id, req.type, req.latitude, req.longitude)}
                  >
                    <Text style={fixerPage.main.style.type}>
                      {req.requesterName}
                    </Text>
                    <Text style={fixerPage.main.style.distance}>
                      {
                        req.distance >= 1 ?
                        parseInt(req.distance)
                        + 'Km and ' 
                        + (parseInt(req.distance*1000) % 1000) 
                        + 'm'
                        :
                        (parseInt(req.distance*1000) % 1000) 
                        + 'm'
                      }
                    </Text>
                    <Text style={fixerPage.main.style.date}>
                      {req.time.toLocaleDateString('en-GB') + ' - ' + req.time.toLocaleTimeString('en-GB')}
                    </Text>
                    {/* <Text style={fixerPage.main.style.status}>
                      {req.responded ? "TAKEN" : "AVAILABLE"}{req.responder ? ' (By You)' : ''}
                    </Text>
                    <Text style={{...fixerPage.main.style.indicator, backgroundColor: (req.responded)?('red'):('lightgreen')}}></Text> */}
                  </TouchableOpacity>
                </View>
              ))}
            </ScrollView>
          ) : (
            <View key={lastReqs[0].key} style={fixerPage.main.style.container}>
              <View style={fixerPage.main.style.area}>
                <Text style={fixerPage.main.style.type}>
                  {lastReqs[0].requesterName}
                </Text>
                <Text style={fixerPage.main.style.distance}>
                  {
                    lastReqs[0].distance >= 1 ?
                    parseInt(lastReqs[0].distance)
                    + 'Km and ' 
                    + (parseInt(lastReqs[0].distance*1000) % 1000) 
                    + 'm'
                    :
                    (parseInt(lastReqs[0].distance*1000) % 1000) 
                    + 'm'
                  }
                </Text>
                <View style={fixerPage.main.buttons.style.vContainer}>
                  <View style={fixerPage.main.buttons.style.hContainer}>
                    <Text onPress={() => callCenter(lastReqs[0].phone)} style={{...fixerPage.main.buttons.style.button, ...fixerPage.main.buttons.style.call}}>Call</Text>
                    <Text onPress={() => openMap(lastReqs[0].latitude, lastReqs[0].longitude)} style={{...fixerPage.main.buttons.style.button, ...fixerPage.main.buttons.style.map}}>Show in Map</Text>
                  </View>
                  <View style={fixerPage.main.buttons.style.hContainer}>
                    <Text onPress={doneRequest} style={{...fixerPage.main.buttons.style.button, ...fixerPage.main.buttons.style.done}}>Done</Text>
                    <Text onPress={cancelRequest} style={{...fixerPage.main.buttons.style.button, ...fixerPage.main.buttons.style.cancel}}>Cancel Job</Text>
                  </View>
                </View>
                <Text style={fixerPage.main.style.date}>
                  {lastReqs[0].time.toLocaleDateString('en-GB') + ' - ' + lastReqs[0].time.toLocaleTimeString('en-GB')}
                </Text>
              </View>
            </View>
          )}
        </>
      )
    }
    else{
      return (
        <>
          <View style={fixerPage.header.style.container}>
            <View style={fixerPage.header.logoutButton.style.container}>
              <TouchableOpacity 
                style={fixerPage.header.logoutButton.style.area}
                onPress={logoutUser}>
                <Text style={fixerPage.header.logoutButton.style.text}>
                  {'LOG OUT'}
                </Text>
              </TouchableOpacity>
            </View>
            {/* <View style={fixerPage.header.userHistoryButton.style.container}>
              <TouchableOpacity 
                style={fixerPage.header.userHistoryButton.style.area}
                onPress={getUserReqHistory}>
                <Text style={fixerPage.header.userHistoryButton.style.text}>
                  {'History'}
                </Text>
              </TouchableOpacity>
            </View> */}
          </View>
          <View>
            <Text style={{padding: 10}}>There is no Recovery request at the moment!</Text>
          </View>
        </>
      )
    }
  }

  if(user && step === states.adminPage){
    return (
      <>
        <Modal
          animationType="fade"
          transparent={true}
          visible={adminQueryModal}
          onRequestClose={() => {
            console.log("Modal has been closed.");
            setAdminQueryModal(false);
            setAdminQueryReady(false);
          }}
        >
          <View style={styles.adminQueryModal}>
            <View style={styles.adminQueryScroll}>
              {adminQueryReady ? (
                <View>
                  <ScrollView style={{marginBottom: 5}}>
                    {adminQueryResults.map(agent =>( 
                      <View style={styles.adminQueryContainer} key={agent.id}>
                        <View style={styles.adminQueryArea}>
                          <Text style={styles.adminQueryName}>{agent.name}</Text>
                          <Text style={styles.adminQueryID}>{agent.id}</Text>
                          <Text style={styles.adminQueryBreakdown}>Breakdowns: {agent.breakdown}</Text>
                          <Text style={styles.adminQueryStart}>Doesn't start: {agent.start}</Text>
                        </View>
                      </View>
                    ))}
                  </ScrollView>
                  <View>
                    <Button onPress={adminCopyAgentsData} title={adminQueryResults.length > 0 ? "Click to copy data as .csv file" : "There was no activity in selected range.\nclose"}/>
                  </View>
                </View>
              ): (
                <View>
                  <Text>Loading</Text>
                </View>
              )}
            </View>
          </View>
        </Modal>
        <View style={{padding: 20,borderBottomColor:"black",borderBottomWidth:1}}>
          <Button onPress={logoutUser} title={'Logout'} />
        </View>
        <ScrollView style={styles.adminUsersContainer}>
          {adminUsers.map(adminUser =>( 
            <View style={styles.adminUserContainer} key={adminUser.id}>
              <View style={styles.adminUserArea}>
                <Text style={styles.adminUserID}>{adminUser.id}</Text>
                <Text style={styles.adminUserName}>{adminUser.name}</Text>
                <Text style={styles.adminUserEMail}>{adminUser.email}</Text>
                <Text style={styles.adminUserPhone}>{adminUser.phone}</Text>
                <View style={styles.adminUserTypeContainer}>
                  <Text style={styles.adminUserTypeText}>{adminUser.type === 3 ? "Admin" : "Agent"}</Text>
                  <Switch
                    disabled={adminUser.type === 3}
                    style={styles.adminUserTypeSwitch}
                    onValueChange={() => changeUserType(adminUser.id, !adminUser.type)}
                    value={adminUser.type === 0 ? false : true}
                  />
                </View>
                <View style={styles.adminUserDeleteContainer}>
                  <Button 
                    disabled={adminUser.type === 3}
                    style={styles.adminUserDeleteButton} 
                    onPress={() => deleteUser(adminUser.id)} 
                    title={'Delete'}
                  />
                </View>
              </View>
            </View>
          ))}
        </ScrollView>
        <View style={{padding: 20, paddingTop: 5,borderTopColor:"black",borderTopWidth:1}}>
          <View style={styles.adminDatePickerLabelContainer}>
            <Text style={styles.adminDatePickerLabelStart}>Start</Text>
            <Text style={styles.adminDatePickerLabelEnd}>End</Text>
          </View>
          <View style={styles.adminDatePickerContainer}>
            <DatePicker
              style={styles.adminDatePickerStart}
              date={queryStartDate}
              placeholder="Please Select Query Start Date"
              onDateChange={date => {
                setQueryStartDate(date)
              }}
            />
            <DatePicker
              style={styles.adminDatePickerEnd}
              date={queryEndDate}
              placeholder="Please Select Query End Date"
              onDateChange={date => {
                setQueryEndDate(date)
              }}
            />
          </View>
          <Button style={{marginTop: 20,}} onPress={getAgentsActivity} title={'Get Agents Activity'}/>
        </View>
      </>
    )
  }

  console.warn(step)
  return (
    <>
      <Text>Your app crashed!{'\n'}Please restart the app and if the problem persist, try clear cache/data!</Text>
    </>
  )
};

const styles = StyleSheet.create({
  revoceryContainer:{
    marginTop: 10,
    marginBottom: 80,
    flex: 1,
    alignContent: "center",
    alignItems: "center",
    justifyContent: 'center',
  },
  revoceryArea:{
    borderRadius: 60,
    borderWidth: 3,
    backgroundColor: '#2196f3',
    borderBottomColor: '#1b7dcc',
    borderLeftColor: '#1b7dcc',
    borderTopColor: '#34a2fa',
    borderRightColor: '#34a2fa',
    width: 120,
    height: 120,
    marginHorizontal: 'auto',
    alignItems: "center",
    justifyContent: 'center',
  },
  revoceryContext:{
    color: 'white',
    textAlign: "center",
  },
  revoceryTypeContainer: {
    alignItems: "center",
    justifyContent: 'center',
  },
  revoceryTypeSwith: {
    width: 40,
    marginTop: 5,
  },
  revoceryTypeText: {

  },
  requestContainer:{
    marginVertical: 0,
  },
  requestArea:{
    borderRadius: 0,
    marginHorizontal: 0,
    borderColor: 'lightblue',
    borderWidth: 1,
    backgroundColor: "transparent",
  },
  requestType:{
    fontSize: 22,
    marginLeft: 10,
    marginTop: 5,
    marginBottom: 20,
  },
  requestDate:{
    position: 'absolute',
    right: 10,
    bottom: 5,
  },
  requestDistance:{
    marginBottom: 5,
    marginLeft: 10,
  },
  requestStatus:{
    position: 'absolute',
    right: 10,
    top: 5,
    fontSize: 12,
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  adminUsersContainer: {
    marginTop: 10,
  },
  adminUserContainer: {
    margin: 5,
    zIndex: 1,
  },
  adminUserArea: {
    borderRadius: 10,
    marginHorizontal: 0,
    borderColor: 'lightblue',
    borderWidth: 1,
    backgroundColor: "transparent",
    zIndex: 2,
  },
  adminUserID: {
    textAlign: 'center',
    fontSize: 20,
  },
  adminUserName: {
    marginTop: 15,
    marginLeft: 10,
  },
  adminUserEMail: {
    marginTop: 10,
    marginLeft: 10,
    marginBottom: 60,
  },
  adminUserPhone: {
    position: "absolute",
    top: 40,
    right: 10,
  },
  adminUserTypeContainer: {
    position: "absolute",
    top: 100,
    right: 10,
  },
  adminUserTypeText:{
    width: 100,
    marginTop: 5,
  },
  adminUserTypeSwitch:{
    position: "absolute",
    top: 0,
    right: 0,
  },
  adminUserDeleteContainer:{
    position: "absolute",
    top: 100,
    left: 10,
    width: 70,
  },
  adminUserDeleteButton:{

  },
  showPasswordText: {
    marginTop: 10,
    marginBottom: 20,
  },
  showPasswordSwitch: {
    position: "absolute",
    top: 10,
    right: 10,
  },
  adminDatePickerContainer: {
    flexDirection: "row",
  },
  adminDatePickerStart: {
    flex: 1,
    marginVertical: 10,
  },
  adminDatePickerEnd: {
    flex: 1,
    marginVertical: 10,
  },
  adminDatePickerLabelContainer: {
    flexDirection: 'row',
  },
  adminDatePickerLabelStart: {
    marginRight: 50,
    textAlign: 'center',
    flex: 1,
    marginTop: 10,
    marginBottom: 2,
  },
  adminDatePickerLabelEnd: {
    marginRight: 50,
    textAlign: 'center',
    flex: 1,
    marginTop: 10,
    marginBottom: 2,
  },
  adminQueryModal: {
    backgroundColor: "#FFFFFF80",
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  adminQueryScroll: {
    margin: 10,
    backgroundColor: "white",
    width: 400,
    paddingVertical: 20,
    borderRadius: 20,
    padding: 0,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  adminQueryContainer: {
    
  },
  adminQueryArea: {
    marginHorizontal: 0,
    borderColor: 'lightblue',
    borderWidth: 1,
    width: 300,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    backgroundColor: "transparent",
  },
  adminQueryID: {
    textAlign: 'center',
  },
  adminQueryName: {
    textAlign: "center",
    fontSize: 20,
    marginTop: 15,
    marginBottom: 15,
    marginLeft: 10,
  },
  adminQueryBreakdown: {
    marginTop: 10,
    marginLeft: 10,
    marginBottom: 10,
  },
  adminQueryStart: {
    position: "absolute",
    top: 88,
    right: 10,
  },
});

const fixerPage = {
  header: {
    style: StyleSheet.create({
      container: {
        backgroundColor: "#edbdadf0",
        height: 60,
      },
    }),
    logoutButton: {
      style: StyleSheet.create({
        container: {
          position: "absolute",
          top: 5,
          left: 5,
          flex: 1,
          alignContent: "center",
          alignItems: "center",
          justifyContent: 'center',
        },
        area: {
          borderRadius: 25,
          padding: 10,
          backgroundColor: '#fabd64',
          width: 90,
          height: 50,
          marginHorizontal: 'auto',
          alignItems: "center",
          justifyContent: 'center',
        },
        text: {},
      }),
    },
    userHistoryButton: {
      style: StyleSheet.create({
        container: {
          position: "absolute",
          top: 5,
          right: 5,
          flex: 1,
          alignContent: "center",
          alignItems: "center",
          justifyContent: 'center',
        },
        area: {
          borderRadius: 25,
          padding: 10,
          backgroundColor: '#fabd64',
          width: 90,
          height: 50,
          marginHorizontal: 'auto',
          alignItems: "center",
          justifyContent: 'center',
        },
        text: {},
      }),
    },
  },
  main: {
    style: StyleSheet.create({
      container: {
        marginHorizontal: 10,
        marginVertical: 5,
      },
      area: {
        borderRadius: 10,
        marginHorizontal: 0,
        borderColor: 'brown',
        borderWidth: 2,
        backgroundColor: "transparent",
      },
      type: {
        fontSize: 22,
        marginLeft: 10,
        marginTop: 10,
        marginBottom: 20,
      },
      date: {
        position: 'absolute',
        right: 10,
        top: 60,
      },
      distance: {
        marginBottom: 5,
        marginLeft: 10,
      },
      status: {
        position: 'absolute',
        right: 50,
        top: 15,
        fontSize: 12,
      },
      indicator: {
        position: 'absolute',
        width: 20,
        height: 20,
        borderRadius: 10,
        right: 10,
        top: 15,
      },
    }),
    buttons:{
      style: StyleSheet.create({
        vContainer: {
          margin: 10,
          width: '94%',
          height: 150,
        },
        hContainer: {
          flex: 1,
          flexDirection: 'row',
        },
        button: {
          flex: 1,
          textAlign: 'center',
          textAlignVertical: 'center',
          margin: 10,
          borderRadius: 30,
          backgroundColor: "red",
          padding: 10,
          textTransform: "uppercase",
        },
        call: {
          backgroundColor: '#ff9375',
        },
        map: {
          backgroundColor: '#ff9375',
        },
        done: {
          backgroundColor: '#95ed0e',
        },
        cancel: {
          backgroundColor: '#ff461c',
        },
      }),
    },
  },
}

const startPage = {
  background: {
    style: StyleSheet.create({
      image:{
        height: "100%",
        resizeMode: "cover",
        justifyContent: "center"
      },
    }),
  },
  main:{
    text:{
      style: StyleSheet.create({
        context:{
          position: "absolute",
          top: 30,
          width: '100%',
          textAlign: "center",
          color: 'white',
          fontSize: 30,
          fontWeight: "bold",
          lineHeight: 50,
        }
      }),
    },
    buttons:{
      style: StyleSheet.create({
        container: {
          flex: 1,
          position: "absolute",
          bottom: 10,
          width: '100%',
          alignItems: "center",
          justifyContent: 'center',
        },
        buttons: {
          borderRadius: 25,
          flex: 1,
          backgroundColor: '#ff9800',
          padding: 20,
          textAlign: 'center',
          marginHorizontal: 20,
          marginVertical: 10,
          width: "90%",
        },
      }),
    },
  },
}

const loginPage = {
  background: {
    style: StyleSheet.create({
      image:{
        height: "100%",
        width: "100%",
        resizeMode: "cover",
        justifyContent: "center"
      },
    }),
  },
  main:{
    inputs:{
      style: StyleSheet.create({
        container: {
          position: "absolute",
          bottom: 20,
          width: "100%",
        },
        input:{
          backgroundColor: '#c7a545',
          borderRadius: 25,
          margin: 10,
          textAlign: 'center',
        }
      }),
      buttons:{
        style: StyleSheet.create({
          container: {
            flex: 1,
            marginHorizontal: 30,
            resizeMode: "cover",
            justifyContent: "center",
          },
          text: {
            marginTop: 10,
            marginBottom: 20,
            color: 'white',
          },
          switch: {
            position: "absolute",
            top: 10,
            right: 10,
          },
          button: {
            borderRadius: 25,
            backgroundColor: '#ff9800',
            paddingVertical: 15,
            paddingHorizontal: 30,
            textAlign: 'center',
            alignSelf: "center",
            marginVertical: 10,            
          },
        }),
        switch:{
          track:{
            false: "#c9c9c9",
            true: "lightgreen",
          },
        },
      },
    },
    text:{
      style: StyleSheet.create({
        context:{
          position: "absolute",
          top: 80,
          width: '100%',
          textAlign: "center",
          color: 'white',
          fontSize: 30,
          fontWeight: "bold",
          lineHeight: 50,
        }
      }),
    },
  },
}

const signupPage = {
  background: {
    style: StyleSheet.create({
      image:{
        height: "100%",
        width: "100%",
        resizeMode: "cover",
        justifyContent: "center"
      },
    }),
  },
  main:{
    inputs:{
      style: StyleSheet.create({
        container: {
          position: "absolute",
          bottom: 20,
          width: "100%",
        },
        input:{
          backgroundColor: '#c7a545',
          borderRadius: 25,
          margin: 10,
          textAlign: 'center',
        }
      }),
      buttons:{
        style: StyleSheet.create({
          container: {
            flex: 1,
            marginHorizontal: 30,
            resizeMode: "cover",
            justifyContent: "center",
          },
          text: {
            marginTop: 10,
            marginBottom: 20,
            color: 'white',
          },
          switch: {
            position: "absolute",
            top: 10,
            right: 10,
          },
          button: {
            borderRadius: 25,
            backgroundColor: '#ff9800',
            paddingVertical: 15,
            paddingHorizontal: 30,
            textAlign: 'center',
            alignSelf: "center",
            marginVertical: 10,            
          },
        }),
        switch:{
          track:{
            false: "#c9c9c9",
            true: "lightgreen",
          },
        },
      },
    },
    text:{
      style: StyleSheet.create({
        context:{
          position: "absolute",
          top: 80,
          width: '100%',
          textAlign: "center",
          color: 'white',
          fontSize: 30,
          fontWeight: "bold",
          lineHeight: 50,
        }
      }),
    },
  },
}

const userPage = {
  header: {
    style: StyleSheet.create({
      container: {
        backgroundColor: "#edbdadf0",
        height: 60,
      },
    }),
    logoutButton: {
      style: StyleSheet.create({
        container: {
          position: "absolute",
          top: 5,
          left: 5,
          flex: 1,
          alignContent: "center",
          alignItems: "center",
          justifyContent: 'center',
        },
        area: {
          borderRadius: 25,
          padding: 10,
          backgroundColor: '#fabd64',
          width: 90,
          height: 50,
          marginHorizontal: 'auto',
          alignItems: "center",
          justifyContent: 'center',
        },
        text: {},
      }),
    },
    userHistoryButton: {
      style: StyleSheet.create({
        container: {
          position: "absolute",
          top: 5,
          right: 5,
          flex: 1,
          alignContent: "center",
          alignItems: "center",
          justifyContent: 'center',
        },
        area: {
          borderRadius: 25,
          padding: 10,
          backgroundColor: '#fabd64',
          width: 90,
          height: 50,
          marginHorizontal: 'auto',
          alignItems: "center",
          justifyContent: 'center',
        },
        text: {},
      }),
    },
  },
  main: {
    background: {
      style: StyleSheet.create({
        image: {
          height: "95%",
          resizeMode: "cover",
          justifyContent: "center"
        },
        container: {
          position: "absolute",
          bottom: 60,
          width: '100%'
        },
      }),
    },
    buttons:{
      left: {
        style: StyleSheet.create({
          container:{
            position: "absolute",
            bottom: 0,
            left: 10,
            flex: 1,
            alignContent: "center",
            alignItems: "center",
            justifyContent: 'center',
          },
        }),
      },
      right: {
        style: StyleSheet.create({
          container:{
            position: "absolute",
            bottom: 0,
            right: 10,
            flex: 1,
            alignContent: "center",
            alignItems: "center",
            justifyContent: 'center',
          },
        }),
      },
      style: StyleSheet.create({
        area: {
          borderRadius: 50,
          backgroundColor: '#fabd64',
          width: 100,
          height: 100,
          marginHorizontal: 'auto',
          alignItems: "center",
          justifyContent: 'center',
        },
        context: {
          color: 'black',
          textAlign: "center",
          textTransform: "uppercase",
        },
      }),
    },
  },
  userHistory:{
    style: StyleSheet.create({
      modal: {
        backgroundColor: "#FFFFFF80",
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
      },
      scroll: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        margin: 10,
        backgroundColor: "white",
        width: 400,
        paddingVertical: 20,
        borderRadius: 20,
        padding: 0,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
      },
      scrollContainer: {
        width: '80%',
        height: '90%',
        flex: 1,
      },
      container: {
        
      },
      area: {
        borderRadius: 0,
        marginHorizontal: 0,
        borderColor: 'lightblue',
        borderWidth: 1,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        backgroundColor: "transparent",
      },
      type: {
        textAlign: "center",
        textAlignVertical: "center",
        textShadowColor: 'rgba(0, 0, 0, 0.25)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 1,
        fontSize: 22,
        marginLeft: 10,
        marginTop: 5,
        marginBottom: 20,
      },
      distance: {
        marginBottom: 5,
        marginLeft: 10,
      },
      date: {
        position: 'absolute',
        right: 10,
        bottom: 5,
      },
    }),
  },
}

export default App;
